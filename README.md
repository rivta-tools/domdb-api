Domdb-api exponerar web-tjänster som hämtar sin data från domain-databasen.

![domains.png](src/main/resources/static/doc/domains.png)

Domdb-api är beroende av domdb-domain.

Tjänsterna är byggda med hjälp av spring-jpa och spring-boot.

För närvarande exponeras tjänsterna servicedomain och servicecontract.

Dokumentation om tjänsterna är skrivna med hjälp av Swagger och hittas vid körning under <din-url>/doc/index.html

### Installation
För installationsinstruktioner av ovanstående:

  * [SystemRequirements](docs/SystemRequirements.md).
  * [DeveloperInstructions](docs/DeveloperInstructions.md)

Uppgradering:

  * [DomdbApiUpgrade](docs/DomdbApiUpgrade.md)