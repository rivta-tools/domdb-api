FROM maven:3-eclipse-temurin-17 AS maven

WORKDIR /opt/build
RUN --mount=type=bind,source=pom.xml,target=pom.xml \
    --mount=type=bind,source=src,target=src \
    --mount=type=cache,target=/root/.m2\
    mvn clean install -U -PdockerizedJar -DskipTests=true

RUN mkdir -p /opt/app
RUN cp /opt/build/target/*.jar /opt/app/app.jar

COPY <<EOF /opt/app/logback.xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration scan="true">
	<include resource="co/elastic/logging/logback/boot/ecs-console-appender.xml"/>
	<contextListener class="ch.qos.logback.classic.jul.LevelChangePropagator">
		<resetJUL>true</resetJUL>
	</contextListener>
	<root level="INFO">
		<appender-ref ref="ECS_JSON_CONSOLE"/>
	</root>
</configuration>
EOF


FROM eclipse-temurin:17-jre-alpine AS app

ENV LOGGING_CONFIG=/opt/app/logback.xml
COPY --from=maven /opt/app/ /opt/app/
RUN adduser ind-app -HD -u 1000
USER ind-app
CMD java -XX:MaxRAMPercentage=75 ${JAVA_OPTS} -jar /opt/app/app.jar
