/**
 * 
 */
package se.rivta.domdb.domdbapi.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import se.rivta.domdb.domain.core.ServiceContract;
import se.rivta.domdb.domdbapi.criteria.ServiceContractCriteria;

/**
 * @author muqkha
 *
 */
public interface ServiceContractService {
	
	List<ServiceContract> findAll(ServiceContractCriteria criteria, Pageable pageRequest);

	ServiceContract findById(Integer id);

}
