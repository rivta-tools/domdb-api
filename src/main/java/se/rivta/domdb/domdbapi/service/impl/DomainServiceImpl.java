package se.rivta.domdb.domdbapi.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.server.ResponseStatusException;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domdbapi.criteria.DomainCriteria;
import se.rivta.domdb.domdbapi.repository.DomainRepository;
import se.rivta.domdb.domdbapi.service.DomainService;


/**
 * @author muqkha
 *
 */
@Service("domainService")
@Transactional(readOnly = true)
public class DomainServiceImpl extends SpecificationBuilder<Domain, DomainCriteria> implements DomainService {
	
	@Autowired
	protected DomainRepository domainRepository;

	@Override
	public List<Domain> findAll(DomainCriteria criteria, Pageable pageRequest) {
		if (criteria.isEmpty()) {
			return domainRepository.findAll(pageRequest).getContent();
		} else {
			Specification<Domain> specification = buildSpecification(criteria);
			return domainRepository.findAll(specification, pageRequest).getContent();
		}
	}

	@Override
	public Domain findById(Integer id){
		if (domainRepository.findById(id).isPresent()) {
			return domainRepository.findById(id).get();
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@Override
	protected void addSpecificationsToQuery(
			LinkedList<Specification<Domain>> specifications, DomainCriteria criteria) {
		if(criteria.getName() != null) {
			specifications.add(likeString("name", criteria.getName()));
		}
		if(criteria.getDomainType() != null){
			specifications.add(matchesInteger("domainType", criteria.getDomainType()));
		}
		if (criteria.getDescription() != null) {
			specifications.add(likeString("description", criteria.getDescription()));
		}
		if (criteria.getSwedishLong() != null) {
			specifications.add(likeString("swedishLong", criteria.getSwedishLong()));
		}
		if (criteria.getSwedishShort() != null) {
			specifications.add(likeString("swedishShort", criteria.getSwedishShort()));
		}
		if (criteria.getOwner() != null) {
			specifications.add(likeString("owner", criteria.getOwner()));
		}
		if (criteria.isHidden() != null) {
			specifications.add(matchesBoolean("hidden", criteria.isHidden()));
		}
		if (criteria.getIssueTrackerUrl() != null) {
			specifications.add(likeString("issueTrackerUrl", criteria.getIssueTrackerUrl()));
		}
		if (criteria.getSourceCodeUrl() != null) {
			specifications.add(likeString("sourceCodeUrl", criteria.getSourceCodeUrl()));
		}
		if (criteria.getInfoPageUrl() != null) {
			specifications.add(likeString("infoPageUrl", criteria.getInfoPageUrl()));
		}
		
	}

}
