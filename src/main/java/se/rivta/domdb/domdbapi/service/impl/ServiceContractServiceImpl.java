package se.rivta.domdb.domdbapi.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.server.ResponseStatusException;
import se.rivta.domdb.domain.core.ServiceContract;
import se.rivta.domdb.domdbapi.criteria.ServiceContractCriteria;
import se.rivta.domdb.domdbapi.repository.ServiceContractRepository;
import se.rivta.domdb.domdbapi.service.ServiceContractService;

/**
 * @author muqkha
 *
 */
@Service("serviceContractService")
@Transactional(readOnly = true)
public class ServiceContractServiceImpl extends SpecificationBuilder<ServiceContract, ServiceContractCriteria> implements ServiceContractService {
	
	@Autowired
	protected ServiceContractRepository serviceContractRepository;
	
	@Override
	public List<ServiceContract> findAll(ServiceContractCriteria criteria, Pageable pageRequest) {
		if (criteria.isEmpty()) {
			return serviceContractRepository.findAll(pageRequest).getContent();
		} else {
			Specification<ServiceContract> specification = buildSpecification(criteria);
			return serviceContractRepository.findAll(specification, pageRequest).getContent();
		}
	}


	@Override
	public ServiceContract findById(Integer id){
		if (serviceContractRepository.findById(id).isPresent()) {
			return serviceContractRepository.findById(id).get();
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@Override
	protected void addSpecificationsToQuery(
			LinkedList<Specification<ServiceContract>> specifications,
			ServiceContractCriteria criteria) {
		if(criteria.getName() != null) {
			specifications.add(likeString("name", criteria.getName()));
		}
		if (criteria.getMajor() != null) {
			specifications.add(matchesShort("major", criteria.getMajor()));
		}
		if (criteria.getMinor() != null) {
			specifications.add(matchesShort("minor", criteria.getMinor()));
		}
		if (criteria.getNamespace() != null) {
			specifications.add(likeString("namespace", criteria.getNamespace()));
		}
	}

}
