package se.rivta.domdb.domdbapi.service.impl;

import java.util.LinkedList;

import javax.persistence.criteria.*;

import org.hibernate.query.criteria.internal.path.SingularAttributePath;
import org.springframework.data.jpa.domain.Specification;

public abstract class SpecificationBuilder<T, S> {

	protected Specification<T> buildSpecification(final S criteria) {
		
		LinkedList<Specification<T>> specifications = new LinkedList<Specification<T>>();

		addSpecificationsToQuery(specifications, criteria);

		Specification<T> specification = specifications.poll();
		
		while (!specifications.isEmpty()) {
			specification = Specification.where(specification).and(specifications.poll());
		}
		
		return specification;
	}
	
	protected abstract void addSpecificationsToQuery(
			LinkedList<Specification<T>> specifications, S criteria);

	protected Specification<T> matchesString(final String parameter, final String string) {
		return new Specification<T>() {
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, 
					CriteriaBuilder builder) {

				return builder.equal(root.get(parameter), string);
			}
	    };
	}
	
	protected Specification<T> likeString(final String parameter, final String string) {
		return new Specification<T>() {
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, 
					CriteriaBuilder builder) {
				Path<Object> x = root.get(parameter);
				SingularAttributePath<Object> path = (SingularAttributePath<Object>)x;
				return builder.like(path.asString(), "%"+string+"%");
			}
	    };
	}
	
	protected Specification<T> matchesBoolean(final String parameter, final boolean bool) {
		return new Specification<T>() {
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, 
					CriteriaBuilder builder) {

				return builder.equal(root.get(parameter), bool);
			}
	    };
	}
	
	protected Specification<T> matchesShort(final String parameter, final Short value) {
		return new Specification<T>() {
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, 
					CriteriaBuilder builder) {

				return builder.equal(root.get(parameter), value);
			}
	    };
	}

	protected Specification<T> matchesInteger(final String parameter, final Integer value) {
		return new Specification<T>() {
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query,
										 CriteriaBuilder builder) {
				return builder.equal(root.get(parameter), value);
			}
		};
	}
}
