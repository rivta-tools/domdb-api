/**
 * 
 */
package se.rivta.domdb.domdbapi.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domdbapi.criteria.DomainCriteria;

/**
 * @author muqkha
 *
 */
public interface DomainService {
	
	List<Domain> findAll(DomainCriteria criteria, Pageable pageRequest);

	Domain findById(Integer id);
}
