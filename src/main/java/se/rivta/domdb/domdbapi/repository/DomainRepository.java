package se.rivta.domdb.domdbapi.repository;

import se.rivta.domdb.domain.core.Domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author muqkha
 *
 */
public interface DomainRepository extends JpaRepository<Domain, Integer>, JpaSpecificationExecutor<Domain>{
//, QueryDslPredicateExecutor<Domain>
}
