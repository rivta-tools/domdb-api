package se.rivta.domdb.domdbapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import se.rivta.domdb.domain.core.DomainType;

/**
 * Created by martul on 2017-10-03.
 */
public interface DomainTypeRepository extends JpaRepository<DomainType, Integer>, JpaSpecificationExecutor<DomainType> {
}
