/**
 * 
 */
package se.rivta.domdb.domdbapi.repository;

import se.rivta.domdb.domain.core.ServiceContract;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author muqkha
 *
 */
public interface ServiceContractRepository extends JpaRepository<ServiceContract, Integer>, JpaSpecificationExecutor<ServiceContract>{
//, QueryDslPredicateExecutor<Domain>
}
