package se.rivta.domdb.domdbapi.basicauthmodule;

import org.springframework.data.jpa.repository.JpaRepository;
import se.rivta.domdb.domdbapi.basicauthmodule.model.ServiceUser;

public interface UserRepository extends JpaRepository<ServiceUser, String> {
}
