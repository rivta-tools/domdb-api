package se.rivta.domdb.domdbapi.basicauthmodule.model.dto;

public class PasswordCrypto {
	public String rawPassword;

	public PasswordCrypto(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public PasswordCrypto() {
	}
}
