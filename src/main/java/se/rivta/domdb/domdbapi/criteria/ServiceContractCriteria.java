/**
 * 
 */
package se.rivta.domdb.domdbapi.criteria;

/**
 * @author muqkha
 *
 */
public class ServiceContractCriteria {
	
	private String name;
	private Short major;
	private Short minor;
	private String namespace;
	
	public ServiceContractCriteria(String name, Short major, Short minor, String namespace) {
		super();
		this.name = name;
		this.major = major;
		this.minor = minor;
		this.namespace = namespace;
	}
	
	public ServiceContractCriteria() {
	}

	public boolean isEmpty() {
		return name == null && major == null && minor == null && namespace == null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Short getMajor() {
		return major;
	}

	public void setMajor(Short major) {
		this.major = major;
	}

	public Short getMinor() {
		return minor;
	}

	public void setMinor(Short minor) {
		this.minor = minor;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

}
