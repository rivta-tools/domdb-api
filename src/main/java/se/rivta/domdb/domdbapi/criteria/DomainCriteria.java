/**
 * 
 */
package se.rivta.domdb.domdbapi.criteria;

/**
 * @author muqkha
 *
 */
public class DomainCriteria {
	
	private String name;
	private String description;
	private String swedishLong;
	private String swedishShort;
	private String owner;
	private Boolean hidden;
	private String issueTrackerUrl;
	private String sourceCodeUrl;
	private String infoPageUrl;
	private Integer domainType;
	
	public DomainCriteria(String name, String description,
						  String swedishLong, String swedishShort, String owner, Boolean hidden,
						  String issueTrackerUrl, String sourceCodeUrl, String infoPageUrl, Integer domainType) {
		super();
		this.name = name;
		this.description = description;
		this.swedishLong = swedishLong;
		this.swedishShort = swedishShort;
		this.owner = owner;
		this.hidden = hidden;
		this.issueTrackerUrl = issueTrackerUrl;
		this.sourceCodeUrl = sourceCodeUrl;
		this.infoPageUrl = infoPageUrl;
		this.domainType = domainType;
	}
	
	public DomainCriteria() {
	}

	public boolean isEmpty() {
		return name == null && description == null && swedishLong == null 
				&& swedishShort == null && hidden == null && owner == null && issueTrackerUrl == null 
				&& sourceCodeUrl == null && infoPageUrl == null && domainType == null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSwedishLong() {
		return swedishLong;
	}

	public void setSwedishLong(String swedishLong) {
		this.swedishLong = swedishLong;
	}

	public String getSwedishShort() {
		return swedishShort;
	}

	public void setSwedishShort(String swedishShort) {
		this.swedishShort = swedishShort;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Boolean isHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public String getIssueTrackerUrl() {
		return issueTrackerUrl;
	}

	public void setIssueTrackerUrl(String issueTrackerUrl) {
		this.issueTrackerUrl = issueTrackerUrl;
	}

	public String getSourceCodeUrl() {
		return sourceCodeUrl;
	}

	public void setSourceCodeUrl(String sourceCodeUrl) {
		this.sourceCodeUrl = sourceCodeUrl;
	}

	public String getInfoPageUrl() {
		return infoPageUrl;
	}

	public void setInfoPageUrl(String infoPageUrl) {
		this.infoPageUrl = infoPageUrl;
	}

	public Integer getDomainType() {
		return domainType;
	}

	public void setDomainType(Integer domainType) {
		this.domainType = domainType;
	}
}
