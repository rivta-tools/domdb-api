package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "review")
@JsonInclude(Include.NON_EMPTY)
public class ReviewDTO {

	@JsonIgnore
	int id;
    ReviewProtocolDTO reviewProtocol;
    ReviewOutcomeDTO reviewOutcome;
    String reportUrl;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public ReviewProtocolDTO getReviewProtocol() {
		return reviewProtocol;
	}
	public void setReviewProtocol(ReviewProtocolDTO reviewProtocol) {
		this.reviewProtocol = reviewProtocol;
	}
	
	public ReviewOutcomeDTO getReviewOutcome() {
		return reviewOutcome;
	}
	public void setReviewOutcome(ReviewOutcomeDTO reviewOutcome) {
		this.reviewOutcome = reviewOutcome;
	}
	
	public String getReportUrl() {
		return reportUrl;
	}
	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}
    

}
