package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import java.util.List;

import se.rivta.domdb.domain.core.RivtaProfile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "interaction")
@JsonInclude(Include.NON_EMPTY)
public class InteractionDTO {

	@JsonIgnore
	int id;
    String name;
    String namespace;
    RivtaProfile rivtaProfile;
    int major;
    int minor;
	ServiceContractDTO initiatorContract;
	ServiceContractDTO responderContract;
	
	@JacksonXmlElementWrapper(localName = "interactionDescriptions")
	@JacksonXmlProperty(localName = "interactionDescription")
	List<InteractionDescriptionDTO> interactionDescriptions;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public int getMajor() {
		return major;
	}
	public void setMajor(int major) {
		this.major = major;
	}
	
	public int getMinor() {
		return minor;
	}
	public void setMinor(int minor) {
		this.minor = minor;
	}
	
	public RivtaProfile getRivtaProfile() {
		return rivtaProfile;
	}
	public void setRivtaProfile(RivtaProfile rivtaProfile) {
		this.rivtaProfile = rivtaProfile;
	}
	
	public ServiceContractDTO getInitiatorContract() {
		return initiatorContract;
	}
	public void setInitiatorContract(ServiceContractDTO initiatorContract) {
		this.initiatorContract = initiatorContract;
	}
	
	public ServiceContractDTO getResponderContract() {
		return responderContract;
	}
	public void setResponderContract(ServiceContractDTO responderContract) {
		this.responderContract = responderContract;
	}
	
	public List<InteractionDescriptionDTO> getInteractionDescriptions() {
		return interactionDescriptions;
	}
	public void setInteractionDescriptions(
			List<InteractionDescriptionDTO> interactionDescriptions) {
		this.interactionDescriptions = interactionDescriptions;
	}
    
}
