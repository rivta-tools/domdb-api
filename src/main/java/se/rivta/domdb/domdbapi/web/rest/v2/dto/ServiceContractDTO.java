package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import se.rivta.domdb.domdbapi.web.rest.v2.api.View;


@JacksonXmlRootElement(localName = "serviceContract")
@JsonInclude(Include.NON_EMPTY)
public class ServiceContractDTO {

	@JsonIgnore
	int id;
	String name;
	short major;
	@JsonInclude
	short minor;
	String namespace;

	@JsonView(View.ForServiceContractAPIController.class)
	DomainLinkDTO domain;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public short getMajor() {
		return major;
	}
	public void setMajor(short major) {
		this.major = major;
	}
	
	public short getMinor() {
		return minor;
	}
	public void setMinor(short minor) {
		this.minor = minor;
	}
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public DomainLinkDTO getDomain() {
		return domain;
	}

	public void setDomain(DomainLinkDTO domain) {
		this.domain = domain;
	}
}
