/**
 * 
 */
package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import se.rivta.domdb.domdbapi.web.rest.v2.api.View;

/**
 * @author muqkha
 *
 */
@JacksonXmlRootElement(localName = "serviceDomain")
@JsonInclude(Include.NON_EMPTY)
public class DomainDTO {
	
	@JsonIgnore
	private Integer id;
	private String name;
    String description;
    String swedishLong;
    String swedishShort;
	private String owner;
	private Boolean hidden;

	@JsonView(View.ForDomainAPIController.class)
	DomainTypeDTO domainType;

	@JacksonXmlElementWrapper(localName = "interactions")
	@JacksonXmlProperty(localName = "interaction")
	List<InteractionDTO> interactions;
	
	@JacksonXmlElementWrapper(localName = "serviceContracts")
	@JacksonXmlProperty(localName = "serviceContract")
	List<ServiceContractDTO> serviceContracts;
	
	@JacksonXmlElementWrapper(localName = "domainVersions")
	@JacksonXmlProperty(localName = "domainVersion")
	List<DomainVersionDTO> versions;
    String issueTrackerUrl;
    String sourceCodeUrl;
    String infoPageUrl;
	
    @JsonIgnore
	public boolean isEmpty() {
		return id == null && name == null && owner == null;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public Boolean isHidden() {
		return hidden;
	}
	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getSwedishLong() {
		return swedishLong;
	}
	public void setSwedishLong(String swedishLong) {
		this.swedishLong = swedishLong;
	}

	public String getSwedishShort() {
		return swedishShort;
	}
	public void setSwedishShort(String swedishShort) {
		this.swedishShort = swedishShort;
	}

	public String getIssueTrackerUrl() {
		return issueTrackerUrl;
	}
	public void setIssueTrackerUrl(String issueTrackerUrl) {
		this.issueTrackerUrl = issueTrackerUrl;
	}

	public String getSourceCodeUrl() {
		return sourceCodeUrl;
	}
	public void setSourceCodeUrl(String sourceCodeUrl) {
		this.sourceCodeUrl = sourceCodeUrl;
	}

	public String getInfoPageUrl() {
		return infoPageUrl;
	}
	public void setInfoPageUrl(String infoPageUrl) {
		this.infoPageUrl = infoPageUrl;
	}

	public List<InteractionDTO> getInteractions() {
		return interactions;
	}
	public void setInteractions(List<InteractionDTO> interactions) {
		this.interactions = interactions;
	}

	public List<ServiceContractDTO> getServiceContracts() {
		return serviceContracts;
	}
	public void setServiceContracts(List<ServiceContractDTO> serviceContracts) {
		this.serviceContracts = serviceContracts;
	}

	public List<DomainVersionDTO> getVersions() {
		return versions;
	}
	public void setVersions(List<DomainVersionDTO> versions) {
		this.versions = versions;
	}

	public DomainTypeDTO getDomainType() {
		return domainType;
	}

	public void setDomainType(DomainTypeDTO domainType) {
		this.domainType = domainType;
	}
}
