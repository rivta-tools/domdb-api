package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import se.rivta.domdb.domdbapi.web.rest.v2.api.View;

/**
 * Created by martul on 2017-05-05.
 */

@JacksonXmlRootElement(localName = "domain")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DomainLinkDTO {
    @JsonView(View.ForServiceContractAPIController.class)
    private String name;

    @JsonView(View.ForServiceContractAPIController.class)
    private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
