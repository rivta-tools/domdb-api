package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * Created by martul on 2017-09-18.
 */
@JacksonXmlRootElement(localName = "domainType")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DomainTypeDTO {
    @JsonIgnore
    private Integer id;

    String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
