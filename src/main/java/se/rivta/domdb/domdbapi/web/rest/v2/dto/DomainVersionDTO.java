package se.rivta.domdb.domdbapi.web.rest.v2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "domain_version")
@JsonInclude(Include.NON_EMPTY)
public class DomainVersionDTO {

	@JsonIgnore
    int id;
    String name;
    String differentDomainName;
    String sourceControlPath;
    String documentsFolder;
    String interactionsFolder;
    String zipUrl;
    boolean hidden;
    
    @JacksonXmlElementWrapper(localName = "descriptionDocuments")
	@JacksonXmlProperty(localName = "descriptionDocument")
    List<DescriptionDocumentDTO> descriptionDocuments;
    
    @JacksonXmlElementWrapper(localName = "interactionDescriptions")
	@JacksonXmlProperty(localName = "interactionDescription")
    List<InteractionDescriptionDTO> interactionDescriptions;
    
    @JacksonXmlElementWrapper(localName = "reviews")
	@JacksonXmlProperty(localName = "review")
    List<ReviewDTO> reviews;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDifferentDomainName() {
		return differentDomainName;
	}
	public void setDifferentDomainName(String differentDomainName) {
		this.differentDomainName = differentDomainName;
	}
	
	public String getSourceControlPath() {
		return sourceControlPath;
	}
	public void setSourceControlPath(String sourceControlPath) {
		this.sourceControlPath = sourceControlPath;
	}
	
	public String getDocumentsFolder() {
		return documentsFolder;
	}
	public void setDocumentsFolder(String documentsFolder) {
		this.documentsFolder = documentsFolder;
	}
	
	public String getInteractionsFolder() {
		return interactionsFolder;
	}
	public void setInteractionsFolder(String interactionsFolder) {
		this.interactionsFolder = interactionsFolder;
	}
	
	public String getZipUrl() {
		return zipUrl;
	}
	public void setZipUrl(String zipUrl) {
		this.zipUrl = zipUrl;
	}
	
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	
	public List<DescriptionDocumentDTO> getDescriptionDocuments() {
		return descriptionDocuments;
	}
	public void setDescriptionDocuments(
			List<DescriptionDocumentDTO> descriptionDocuments) {
		this.descriptionDocuments = descriptionDocuments;
	}
	
	public List<InteractionDescriptionDTO> getInteractionDescriptions() {
		return interactionDescriptions;
	}
	public void setInteractionDescriptions(
			List<InteractionDescriptionDTO> interactionDescriptions) {
		this.interactionDescriptions = interactionDescriptions;
	}
	
	public List<ReviewDTO> getReviews() {
		return reviews;
	}
	public void setReviews(List<ReviewDTO> reviews) {
		this.reviews = reviews;
	}
    
}
