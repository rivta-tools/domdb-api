/**
 *
 */
package se.rivta.domdb.domdbapi.web.rest.v2.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.base.Splitter;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.web.*;

import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domdbapi.criteria.DomainCriteria;
import se.rivta.domdb.domdbapi.service.DomainService;
import se.rivta.domdb.domdbapi.web.exception.ResourceNotFoundException;
import se.rivta.domdb.domdbapi.web.rest.v2.dto.DomainDTO;
import se.rivta.domdb.domdbapi.web.rest.v2.dto.list.DomainListDTO;

@RestController()
@RequestMapping(value = {
    "/v2/servicedomains",
    "/v2/servicedomains.xml",
    "/v2/servicedomains.json"
})
public class DomainAPIController {

  private final Logger log = LoggerFactory.getLogger(DomainAPIController.class);

  private static final Splitter SPLITTER = Splitter.on(',').trimResults().omitEmptyStrings();
  private static final String NAME = "name";
  private static final String HIDDEN = "hidden";
  private static final String DESCRIPTION = "description";
  private static final String OWNER = "owner";
  private static final String SWEDISH_LONG = "swedishLong";
  private static final String SWEDISH_SHORT = "swedishShort";
  private static final String INTERACTIONS = "interactions";
  private static final String SERVICE_CONTRACTS = "serviceContracts";
  private static final String DOMAIN_VERSIONS = "domainVersions";
  private static final String ISSUE_TRACKER_URL = "issueTrackerUrl";
  private static final String SOURCE_URL = "sourceCodeUrl";
  private static final String INFO_PAGE_URL = "infoPageUrl";

  @Autowired
  private DozerBeanMapper mapper;

  @Autowired
  private DomainService domainService;

  private List<DomainDTO> getAll(
      String name,
      String description,
      String swedishLong,
      String swedishShort,
      String owner,
      Boolean hidden,
      Integer domainType,
      String issueTrackerUrl,
      String sourceCodeUrl,
      String infoPageUrl,
      String includeFields,
      Pageable pageRequest) {

    DomainCriteria criteria = new DomainCriteria(name, description,
        swedishLong, swedishShort, owner, hidden,
        issueTrackerUrl, sourceCodeUrl, infoPageUrl, domainType);
    List<Domain> domains = domainService.findAll(criteria, pageRequest);

    List<DomainDTO> result = new ArrayList<>();
    for (Domain cp : domains) {
      DomainDTO domainDTO = toDTO(cp);
      excludeFields(includeFields, domainDTO);
      result.add(domainDTO);
    }
    return result;
  }

  /**
   * GET endpoint /connectionPoints for requesting connectionPoints by filter parameters.
   *
   * @param name            Filter parameter.
   * @param description     Filter parameter.
   * @param swedishLong     Filter parameter.
   * @param swedishShort    Filter parameter.
   * @param owner           Filter parameter.
   * @param hidden          Filter parameter.
   * @param domainType      Filter parameter.
   * @param issueTrackerUrl Filter parameter.
   * @param sourceCodeUrl   Filter parameter.
   * @param infoPageUrl     Filter parameter.
   * @param include         Filter parameter.
   * @param pageRequest     Pagination parameter, supports: size, page, sort=field,(ASC|DESC)
   * @return All matching connectionPoints as a JSON document.
   */
  @JsonView(View.ForDomainAPIController.class)
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<DomainDTO> getAllAsJson(
      @RequestParam(required = false) String name,
      @RequestParam(required = false) String description,
      @RequestParam(required = false) String swedishLong,
      @RequestParam(required = false) String swedishShort,
      @RequestParam(required = false) String owner,
      @RequestParam(required = false) Boolean hidden,
      @RequestParam(required = false) Integer domainType,
      @RequestParam(required = false) String issueTrackerUrl,
      @RequestParam(required = false) String sourceCodeUrl,
      @RequestParam(required = false) String infoPageUrl,
      @RequestParam(required = false) String include,
      @PageableDefault(size = Integer.MAX_VALUE) Pageable pageRequest) {
    log.debug("REST request to get all Domains as json");

    return getAll(name, description,
        swedishLong, swedishShort, owner, hidden,
        domainType, issueTrackerUrl, sourceCodeUrl, infoPageUrl, include, pageRequest);

  }

  /**
   * GET endpoint /connectionPoints for requesting connectionPoints by filter parameters.
   *
   * @param name            Filter parameter.
   * @param description     Filter parameter.
   * @param swedishLong     Filter parameter.
   * @param swedishShort    Filter parameter.
   * @param owner           Filter parameter.
   * @param hidden          Filter parameter.
   * @param domainType      Filter parameter.
   * @param issueTrackerUrl Filter parameter.
   * @param sourceCodeUrl   Filter parameter.
   * @param infoPageUrl     Filter parameter.
   * @param include         Filter parameter.
   * @param pageRequest     Pagination parameter, supports: size, page, sort=field,(ASC|DESC)
   * @return All matching connectionPoints as an XML document.
   */
  @JsonView(View.ForDomainAPIController.class)
  @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
  public DomainListDTO getAllAsXml(
      @RequestParam(required = false) String name,
      @RequestParam(required = false) String description,
      @RequestParam(required = false) String swedishLong,
      @RequestParam(required = false) String swedishShort,
      @RequestParam(required = false) String owner,
      @RequestParam(required = false) Boolean hidden,
      @RequestParam(required = false) Integer domainType,
      @RequestParam(required = false) String issueTrackerUrl,
      @RequestParam(required = false) String sourceCodeUrl,
      @RequestParam(required = false) String infoPageUrl,
      @RequestParam(required = false) String include,
      @PageableDefault(size = Integer.MAX_VALUE) Pageable pageRequest) {
    log.debug("REST request to get all Domains as xml");

    return new DomainListDTO(
        getAll(
            name,
            description,
            swedishLong,
            swedishShort,
            owner,
            hidden,
            domainType,
            issueTrackerUrl,
            sourceCodeUrl,
            infoPageUrl,
            include,
            pageRequest)
    );
  }

  @JsonView(View.ForDomainAPIController.class)
  @GetMapping(value = {"/{id}", "/{id}.json", "/{id}.xml"}, produces = {
      MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  public DomainDTO getDomain(@PathVariable Integer id) {
    log.debug("REST request to get Domain with id = {}", id);
    Domain domain = domainService.findById(id);
    if (domain == null) {
      log.debug("Domain with id {} not found", id);
      throw new ResourceNotFoundException("Domain with id = " + id + " not found");
    }
    return toDTO(domain);
  }

  private DomainDTO toDTO(Domain domain) {
    return mapper.map(domain, DomainDTO.class);
  }

  private DomainDTO excludeFields(String fieldsForInclude, DomainDTO serviceContractDTO) {
    if (fieldsForInclude == null) return serviceContractDTO;

    List<String> includeFields = SPLITTER.splitToList(fieldsForInclude);

    if (!includeFields.contains(DESCRIPTION)) {
      serviceContractDTO.setDescription(null);
    }

    if (!includeFields.contains(NAME)) {
      serviceContractDTO.setName(null);
    }

    if (!includeFields.contains(SWEDISH_LONG)) {
      serviceContractDTO.setSwedishLong(null);
    }

    if (!includeFields.contains(SWEDISH_SHORT)) {
      serviceContractDTO.setSwedishShort(null);
    }

    if (!includeFields.contains(OWNER)) {
      serviceContractDTO.setOwner(null);
    }

    if (!includeFields.contains(INTERACTIONS)) {
      serviceContractDTO.setInteractions(null);
    }

    if (!includeFields.contains(DOMAIN_VERSIONS)) {
      serviceContractDTO.setVersions(null);
    }

    if (!includeFields.contains(SERVICE_CONTRACTS)) {
      serviceContractDTO.setServiceContracts(null);
    }

    if (!includeFields.contains(ISSUE_TRACKER_URL)) {
      serviceContractDTO.setIssueTrackerUrl(null);
    }

    if (!includeFields.contains(SOURCE_URL)) {
      serviceContractDTO.setSourceCodeUrl(null);
    }

    if (!includeFields.contains(INFO_PAGE_URL)) {
      serviceContractDTO.setInfoPageUrl(null);
    }

    if (!includeFields.contains(HIDDEN)) {
      serviceContractDTO.setHidden(null);
    }

    return serviceContractDTO;
  }

}
