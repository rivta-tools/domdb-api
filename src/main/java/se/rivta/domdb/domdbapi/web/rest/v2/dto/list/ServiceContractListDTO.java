/**
 * 
 */
package se.rivta.domdb.domdbapi.web.rest.v2.dto.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import se.rivta.domdb.domdbapi.web.rest.v2.dto.ServiceContractDTO;

/**
 * @author muqkha
 *
 */
@JacksonXmlRootElement(localName="serviceContracts")
public class ServiceContractListDTO {

	@JacksonXmlProperty(localName = "serviceContract")
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<ServiceContractDTO> serviceContracts = new ArrayList<>();

	public ServiceContractListDTO(List<ServiceContractDTO> serviceContracts) {
		super();
		this.serviceContracts = serviceContracts;
	}

	@XmlElement(name = "serviceContract")
	public List<ServiceContractDTO> getServiceContracts() {
		return serviceContracts;
	}

	public void setServiceContracts(List<ServiceContractDTO> serviceContracts) {
		this.serviceContracts = serviceContracts;
	}
}