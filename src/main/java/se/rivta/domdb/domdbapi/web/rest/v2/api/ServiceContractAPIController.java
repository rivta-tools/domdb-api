/**
 *
 */
package se.rivta.domdb.domdbapi.web.rest.v2.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import se.rivta.domdb.domain.core.ServiceContract;
import se.rivta.domdb.domdbapi.criteria.ServiceContractCriteria;
import se.rivta.domdb.domdbapi.service.ServiceContractService;
import se.rivta.domdb.domdbapi.web.exception.ResourceNotFoundException;
import se.rivta.domdb.domdbapi.web.rest.v2.dto.ServiceContractDTO;
import se.rivta.domdb.domdbapi.web.rest.v2.dto.list.ServiceContractListDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController()
@RequestMapping(value = {
    "/v2/servicecontracts",
    "/v2/servicecontracts.xml",
    "/v2/servicecontracts.json"
})
public class ServiceContractAPIController {

  private final Logger log = LoggerFactory.getLogger(ServiceContractAPIController.class);

  @Autowired
  private DozerBeanMapper mapper;

  @Autowired
  private ServiceContractService serviceContractService;

  private List<ServiceContractDTO> getAll(
      String name,
      Short major,
      Short minor,
      String namespace,
      Pageable pageRequest) {

    ServiceContractCriteria criteria = new ServiceContractCriteria(name, major, minor, namespace);
    List<ServiceContract> serviceContracts = serviceContractService.findAll(criteria, pageRequest);

    List<ServiceContractDTO> result = new ArrayList<>();
    for (ServiceContract cp : serviceContracts) {
      result.add(toDTO(cp));
    }
    return result;
  }

  /**
   * GET endpoint /connectionPoints for requesting connectionPoints by filter parameters.
   * @param name Filter parameter.
   * @param major Filter parameter.
   * @param minor Filter parameter.
   * @param namespace Filter parameter.
   * @param pageRequest Pagination parameter.
   * @return All matching connectionPoints as a JSON document.
   */
  @JsonView(View.ForServiceContractAPIController.class)
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<ServiceContractDTO> getAllAsJson(
      @RequestParam(required = false) String name,
      @RequestParam(required = false) Short major,
      @RequestParam(required = false) Short minor,
      @RequestParam(required = false) String namespace,
      @PageableDefault(size = Integer.MAX_VALUE) Pageable pageRequest
  ) {
    log.debug("REST request to get all ServiceContract as json");

    return getAll(name, major, minor, namespace, pageRequest);

  }

  /**
   * GET endpoint /connectionPoints for requesting connectionPoints by filter parameters.
   * @param name Filter parameter.
   * @param major Filter parameter.
   * @param minor Filter parameter.
   * @param namespace Filter parameter.
   * @param pageRequest Pagination parameter.
   * @return All matching connectionPoints as an XML document.
   */
  @JsonView(View.ForServiceContractAPIController.class)
  @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
  public ServiceContractListDTO getAllAsXml(
      @RequestParam(required = false) String name,
      @RequestParam(required = false) Short major,
      @RequestParam(required = false) Short minor,
      @RequestParam(required = false) String namespace,
      @PageableDefault(size = Integer.MAX_VALUE) Pageable pageRequest
  ) {
    log.debug("REST request to get all ServiceContract as xml");

    return new ServiceContractListDTO(
    		getAll(name, major, minor, namespace, pageRequest)
		);

  }

  @JsonView(View.ForServiceContractAPIController.class)
  @GetMapping(value = {"/{id}", "/{id}.json", "/{id}.xml"}, produces = {
      MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  public ServiceContractDTO getServiceContract(
      @PathVariable Integer id,
      @RequestParam(required = false) String include) {
    log.debug("REST request to get ServiceContract with id = {}", id);
    ServiceContract contract = serviceContractService.findById(id);
    if (contract == null) {
      log.debug("ServiceContract with id {} not found", id);
      throw new ResourceNotFoundException("ServiceContract with id = " + id + " not found");
    }

    return toDTO(contract);
  }


  private ServiceContractDTO toDTO(ServiceContract serviceContract) {
    ServiceContractDTO dto = mapper.map(serviceContract, ServiceContractDTO.class);
    String link = linkTo(methodOn(DomainAPIController.class).getDomain(serviceContract.getDomain().getId())).toString();
    dto.getDomain().setLink(link);
    return dto;
  }

}
