/**
 * 
 */
package se.rivta.domdb.domdbapi.web.rest.v2.dto.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import se.rivta.domdb.domdbapi.web.rest.v2.dto.DomainDTO;

/**
 * @author muqkha
 *
 */
@JacksonXmlRootElement(localName="serviceDomains")
public class DomainListDTO {

	@JacksonXmlProperty(localName = "serviceDomain")
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<DomainDTO> domains = new ArrayList<>();

	public DomainListDTO(List<DomainDTO> domains) {
		super();
		this.domains = domains;
	}

	@XmlElement(name = "domain")
	public List<DomainDTO> getDomains() {
		return domains;
	}

	public void setDomains(List<DomainDTO> domains) {
		this.domains = domains;
	}
}