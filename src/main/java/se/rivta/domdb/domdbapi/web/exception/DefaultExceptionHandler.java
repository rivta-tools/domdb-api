package se.rivta.domdb.domdbapi.web.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by martul on 2017-05-04.
 */

@ControllerAdvice
public class DefaultExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ProblemDetail handleResourceNotFound(HttpServletRequest request, ResourceNotFoundException e) {
        ProblemDetail error = new ProblemDetail();
        buildErrorMessage(request, e, HttpStatus.NOT_FOUND, error);

        return error;
    }

    private void buildErrorMessage(HttpServletRequest request, Exception e, HttpStatus status, ProblemDetail error) {
        try {
            error.setType(new URI("https://httpstatuses.com/" + status.value()));
        } catch (URISyntaxException e1) {
            log.error("Unable to set error type", e);
        }
        error.setTitle(status.getReasonPhrase());
        error.setStatus(status.value());
        error.setDetail(e.getMessage());
        String url = request.getRequestURL().toString();
        if (request.getQueryString() != null) {
            url = url + "?" + request.getQueryString();
        }
        error.setInstance(url);
    }
}
