package se.rivta.domdb.domdbapi.web.rest.v2.dto;
import java.util.Date;

import se.rivta.domdb.domain.core.DescriptionDocumentType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "description_document")
@JsonInclude(Include.NON_EMPTY)
public class DescriptionDocumentDTO {

	@JsonIgnore
    int id;
    String fileName;
    
    @JsonFormat(pattern="yyyy-MM-dd")
    Date lastChangedDate;
    DescriptionDocumentType documentType;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public Date getLastChangedDate() {
		return lastChangedDate;
	}
	public void setLastChangedDate(Date lastChangedDate) {
		this.lastChangedDate = lastChangedDate;
	}
	
	public DescriptionDocumentType getDocumentType() {
		return documentType;
	}
	public void setDocumentType(DescriptionDocumentType documentType) {
		this.documentType = documentType;
	}
    
}
