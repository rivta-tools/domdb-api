package se.rivta.domdb.domdbapi.web.rest.v2.api;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by martul on 2017-10-06.
 */
public class BaseTest {

    private MockMvc mockMvc;

    void contains(String content, String data) {
        assertTrue(content.contains(data));
    }

    void containsNot(String content, String data) {
        assertFalse(content.contains(data));
    }

    MappingJackson2HttpMessageConverter jacksonMessageConverter() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper);
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.ALL);
        converter.setSupportedMediaTypes(mediaTypes);
        return converter;
    }

    protected String doRequestForXML(String url) throws Exception,
            UnsupportedEncodingException {
        MockHttpServletResponse response = mockMvc.perform(get(url).accept(MediaType.APPLICATION_XML_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML)).andReturn().getResponse();
        return response.getContentAsString();
    }

    protected String doRequestForXMLCharsetUTF(String url) throws Exception,
            UnsupportedEncodingException {
        MockHttpServletResponse response = mockMvc.perform(get(url).accept(MediaType.APPLICATION_XML_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML + ";charset=UTF-8")).andReturn().getResponse();
        return response.getContentAsString();
    }

    protected String doRequestForJSON(String url) throws Exception,
            UnsupportedEncodingException {
        MockHttpServletResponse response = mockMvc.perform(get(url).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        return response.getContentAsString();
    }

    protected void setup(Object controller) {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .setMessageConverters(jacksonMessageConverter())
//                .setViewResolvers(new ViewResolver() {
//                    @Override
//                    public org.springframework.web.servlet.View resolveViewName(String viewName, Locale locale) throws Exception {
//                        return new MappingJackson2JsonView();
//                    }
//                })
                .build();
    }
}
