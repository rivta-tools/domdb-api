package se.rivta.domdb.domdbapi.web.rest.v2.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;

import se.rivta.domdb.domain.core.*;
import se.rivta.domdb.domdbapi.repository.DomainTypeRepository;
import se.rivta.domdb.domdbapi.service.DomainTestService;

public abstract class DomainAPIControllerTestBase extends BaseTest{

	@Autowired
	private DomainTestService domainTestService;

	@Autowired
	private DomainTypeRepository domainTypeRepository;
	
	@Autowired
	protected DomainAPIController controller;

	//test data
	protected DomainType domainType;
	protected DomainType domainType2;
	protected Domain domain;
	protected Domain domain2;
	protected Domain domain3;


	protected void prepareDatabase() {
		domainType = new DomainType();
		domainType.setName("DomainName");
		domainType.setDescription("Descr");
		domainType.setPluralName("DomainName");
		domainTypeRepository.save(domainType);

		domainType2 = new DomainType();
		domainType2.setName("DomainName2");
		domainType2.setDescription("Descr2");
		domainType2.setPluralName("DomainName2");
		domainTypeRepository.save(domainType2);

		domain = new Domain();
		domain.setName("TestName1");
		domain.setDescription("Desc1");
		domain.setSwedishLong("SwedishLong1");
		domain.setSwedishShort("SwedishShort1");
		domain.setOwner("Owner1");
		domain.setHidden(false);
		domain.setIssueTrackerUrl("IssueTracker1");
		domain.setSourceCodeUrl("SourceCode1");
		domain.setInfoPageUrl("InfoPage1");
		domain.setDomainType(domainType);
		domainType.getDomains().add(domain);
		domainTestService.save(domain);

		domain2 = new Domain();
		domain2.setName("TestName2");
		domain2.setDescription("Desc2");
		domain2.setSwedishLong("SwedishLong2");
		domain2.setSwedishShort("SwedishShort2");
		domain2.setOwner("Owner2");
		domain2.setHidden(false);
		domain2.setIssueTrackerUrl("IssueTracker2");
		domain2.setSourceCodeUrl("SourceCode2");
		domain2.setInfoPageUrl("InfoPage2");
		domain2.setDomainType(domainType);
		domainType.getDomains().add(domain2);
		domainTestService.save(domain2);

		domain3 = new Domain();
		domain3.setName("TestName3");
		domain3.setDescription("Desc3");
		domain3.setSwedishLong("SwedishLong3");
		domain3.setSwedishShort("SwedishShort3");
		domain3.setOwner("Owner3");
		domain3.setHidden(true);
		domain3.setIssueTrackerUrl("IssueTracker3");
		domain3.setSourceCodeUrl("SourceCode3");
		domain3.setInfoPageUrl("InfoPage3");
		domain3.setDomainType(domainType2);
		domainType2.getDomains().add(domain3);

		Set<Interaction> interactionSet = new HashSet<>();
		Interaction interaction = new Interaction();

		interaction.setName("testName1");
		interaction.setNamespace("testNamespace1");
		interaction.setRivtaProfile(RivtaProfile.Unknown);
		interaction.setDomain(domain3);
		interactionSet.add(interaction);
		domain3.setInteractions(interactionSet);

		Set<ServiceContract> serviceContractSet = new HashSet<>();
		ServiceContract serviceContract = new ServiceContract();

		serviceContract.setNamespace("testNamespace1");
		serviceContract.setDomain(domain3);
		serviceContract.setName("testName1");
		serviceContractSet.add(serviceContract);
		domain3.setServiceContracts(serviceContractSet);

		Set<DomainVersion> domainVersions = new HashSet<>();
		DomainVersion domainVersion = new DomainVersion();
		domainVersion.setName("testName1");
		domainVersion.setDomain(domain3);
		domainVersions.add(domainVersion);
		domain3.setVersions(domainVersions);
		domainTestService.save(domain3);
	}

	protected void clearDatabase(){
		//Alla underliggande objekt tas bort automatiskt med hjälpa av cascade
		domainTypeRepository.delete(domainType);
		domainTypeRepository.delete(domainType2);
	}
}
