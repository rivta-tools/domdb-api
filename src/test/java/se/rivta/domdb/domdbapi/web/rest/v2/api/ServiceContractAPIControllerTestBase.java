package se.rivta.domdb.domdbapi.web.rest.v2.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.springframework.beans.factory.annotation.Autowired;

import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domain.core.ServiceContract;
import se.rivta.domdb.domdbapi.service.DomainTestService;
import se.rivta.domdb.domdbapi.service.ServiceContractTestService;

public abstract class ServiceContractAPIControllerTestBase extends BaseTest{

	@Autowired
	ServiceContractTestService serviceContractTestService;
	
	@Autowired
	DomainTestService domainTestService;
	
	@Autowired
	ServiceContractAPIController controller;


	protected ServiceContract serviceContract;
	protected ServiceContract serviceContract2;
	protected ServiceContract serviceContract3;
	protected Domain domain;
	protected Domain domain2;
	protected Domain domain3;
	

	protected void prepareDatabase() {
		serviceContract = new ServiceContract();
		serviceContract.setName("TestName1");
		serviceContract.setMajor((short)1);
		serviceContract.setMinor((short)1);
		serviceContract.setNamespace("Namespace1");
		domain = new Domain();
		domain.setName("DomainName1");
		domain = domainTestService.save(domain);
		domain.getServiceContracts().add(serviceContract);
		serviceContract.setDomain(domain);
		serviceContractTestService.save(serviceContract);

		serviceContract2 = new ServiceContract();
		serviceContract2.setName("TestName2");
		serviceContract2.setMajor((short)1);
		serviceContract2.setMinor((short)2);
		serviceContract2.setNamespace("Namespace2");
		domain2 = new Domain();
		domain2.setName("DomainName2");
		domain2 = domainTestService.save(domain2);
		domain2.getServiceContracts().add(serviceContract2);
		serviceContract2.setDomain(domain2);
		serviceContractTestService.save(serviceContract2);

		serviceContract3 = new ServiceContract();
		serviceContract3.setName("TestName3");
		serviceContract3.setMajor((short)2);
		serviceContract3.setMinor((short)1);
		serviceContract3.setNamespace("Namespace3");
		domain3 = new Domain();
		domain3.setName("DomainName3");
		domain3 = domainTestService.save(domain3);
		domain3.getServiceContracts().add(serviceContract3);
		serviceContract3.setDomain(domain3);
		serviceContractTestService.save(serviceContract3);
	}

	protected void clearDatabase(){
		//Alla underliggande objekt tas bort automatiskt med hjälpa av cascade
		domainTestService.delete(domain);
		domainTestService.delete(domain2);
		domainTestService.delete(domain3);
	}
}
