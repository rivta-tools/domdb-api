package se.rivta.domdb.domdbapi.web.rest.v2.api;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.annotation.PostConstruct;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se.rivta.domdb.domdbapi.Application;

@ActiveProfiles(profiles = "dev")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
@Rollback(false)
public class DomainAPIControllerTest extends DomainAPIControllerTestBase{

	@PostConstruct
	public void setup() {
		super.setup(controller);
	}

	@Before
	public void prepareDatabase() {
		super.prepareDatabase();
	}

	@After
	public void clearDatabase(){
		super.clearDatabase();
	}


	@Test
	public void filterOnHiddenAndName() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?hidden=false&name=" + domain2.getName());

		containsNot(content,  domain.getName());
		contains(content,  domain2.getName());
		containsNot(content,  domain3.getName());
	}

	@Test
	public void filterOnName() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?name=" + domain.getName());

		contains(content,  domain.getName());
		containsNot(content,  domain2.getName());
		containsNot(content,  domain3.getName());
	}

	@Test
	public void filterOnDescription() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?description=" + domain3.getDescription());

		containsNot(content, domain.getDescription());
		containsNot(content, domain2.getDescription());
		contains(content, domain3.getDescription());
		contains(content, domain3.getName());
	}

	@Test
	public void filterOnSwedishLong() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?swedishLong="+ domain2.getSwedishLong());

		containsNot(content,  domain.getSwedishLong());
		contains(content,  domain2.getSwedishLong());
		contains(content,  domain2.getName());
		containsNot(content,  domain3.getSwedishLong());
	}

	@Test
	public void filterOnSwedishShort() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?swedishShort=" + domain2.getSwedishShort());

		containsNot(content, domain.getSwedishShort());
		contains(content, domain2.getSwedishShort());
		contains(content, domain2.getName());
		containsNot(content, domain3.getSwedishShort());
	}

	@Test
	public void filterOnSwedishOwner() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?owner=" + domain3.getOwner());

		containsNot(content, domain.getOwner());
		containsNot(content, domain2.getOwner());
		contains(content, domain3.getName());
		contains(content, domain3.getOwner());
	}

	@Test
	public void filterOnHidden() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?hidden=false");
		if(domain.isHidden()){
			containsNot(content, domain.getName());
		} else {
			contains(content, domain.getName());
		}

		if(domain2.isHidden()){
			containsNot(content, domain2.getName());
		} else {
			contains(content, domain2.getName());
		}

		if(domain3.isHidden()){
			containsNot(content, domain3.getName());
		} else {
			contains(content, domain3.getName());
		}
	}

	@Test
	public void filterOnIssueTrackerUrl() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?issueTrackerUrl=" + domain2.getIssueTrackerUrl());

		containsNot(content, domain.getIssueTrackerUrl());
		contains(content, domain2.getIssueTrackerUrl());
		contains(content, domain2.getName());
		containsNot(content, domain3.getIssueTrackerUrl());
	}

	@Test
	public void filterOnSourceCodeUrl() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?sourceCodeUrl=" + domain.getSourceCodeUrl());

		contains(content,  domain.getSourceCodeUrl());
		contains(content, domain.getName());
		containsNot(content,  domain2.getSourceCodeUrl());
		containsNot(content,  domain3.getSourceCodeUrl());
	}

	@Test
	public void filterOnInfoPageUrl() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?infoPageUrl=" + domain3.getInfoPageUrl());

		containsNot(content,  domain.getInfoPageUrl());
		containsNot(content,  domain2.getInfoPageUrl());
		contains(content, domain3.getInfoPageUrl());
		contains(content, domain3.getName());
	}

	@Test
	public void getAllAsJson() throws Exception {
		String content = doRequestForJSON("/v2/servicedomains.json");

		contains(content, domain.getName());
		contains(content, domain2.getName());
		contains(content, domain3.getName());
	}

	@Test
	public void getAllAsXml() throws Exception {
		String content = doRequestForXML("/v2/servicedomains");

		contains(content, domain.getName());
		contains(content, domain2.getName());
		contains(content, domain3.getName());
	}

	@Test
	public void getOne() throws Exception{
		String content = doRequestForXML("/v2/servicedomains/" + domain.getId());

		contains(content, domain.getName());
		containsNot(content, domain2.getName());
		containsNot(content, domain3.getName());
	}

	@Test
	public void getOneAsJson() throws Exception{
		String content = doRequestForXML("/v2/servicedomains.json/"+ domain2.getId());

		containsNot(content, domain.getName());
		contains(content, domain2.getName());
		containsNot(content, domain3.getName());
	}

	@Test
	public void getAllPagingSortByName() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?size=1&page=0&sort=name");

		contains(content, domain.getName());
		containsNot(content, domain2.getName());
		containsNot(content, domain3.getName());
	}

	@Test
	public void getAllByDomainType1() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?domainType=" + domainType.getId());

		contains(content, domain.getName());
		contains(content, domain2.getName());
		containsNot(content, domain3.getName());
	}

	@Test
	public void getAllByDomainType2() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?domainType=" + domainType2.getId());

		containsNot(content, domain.getName());
		containsNot(content, domain2.getName());
		contains(content, domain3.getName());
	}

	@Test
	public void getAllByDomainTypeNotExists() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?domainType=3");

		containsNot(content, domain.getName());
		containsNot(content, domain2.getName());
		containsNot(content, domain3.getName());
	}

	@Test
	public void getAllPagingSortByNameDESC() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?size=2&page=1&sort=description,DESC");

		contains(content, domain.getName());
		containsNot(content, domain2.getName());
		containsNot(content, domain3.getName());
		}

	@Test
	public void getAllWithFilter() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?include=name," +
				"owner,swedishLong,swedishShort," +
				"interactions,serviceContracts,domainVersions," +
				"issueTrackerUrl,sourceCodeUrl,infoPageUrl");

		contains(content, domain.getName());
		contains(content, domain2.getName());
		contains(content, domain3.getName());

		contains(content, "name");
		contains(content, "owner");
		contains(content, "swedishLong");
		contains(content, "swedishShort");
		contains(content, "interactions");
		contains(content, "serviceContracts");
		contains(content, "versions");
		contains(content, "issueTrackerUrl");
		contains(content, "sourceCodeUrl");
		contains(content, "infoPageUrl");
	}

	@Test
	public void getAllWithFilter2() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?include=" +
				"name,owner,swedishLong," +
				"interactions,serviceContracts," +
				"issueTrackerUrl,infoPageUrl");

		contains(content, domain.getName());
		contains(content, domain2.getName());
		contains(content, domain3.getName());

		contains(content, "name");
		contains(content, "owner");
		contains(content, "swedishLong");
		containsNot(content, "swedishShort");
		contains(content, "interactions");
		contains(content, "serviceContracts");
		containsNot(content, "versions");
		contains(content, "issueTrackerUrl");
		containsNot(content, "sourceCodeUrl");
		contains(content, "infoPageUrl");

	}
	@Test
	public void getAllWithFilter3() throws Exception {
		String content = doRequestForXML("/v2/servicedomains?include=name");

		contains(content, domain.getName());
		contains(content, domain2.getName());
		contains(content, domain3.getName());

		containsNot(content, "owner");
		containsNot(content, "swedishLong");
		containsNot(content, "swedishShort");
		containsNot(content, "interactions");
		containsNot(content, "serviceContracts");
		containsNot(content, "versions");
		containsNot(content, "issueTrackerUrl");
		containsNot(content, "sourceCodeUrl");
		containsNot(content, "infoPageUrl");
	}

}
