package se.rivta.domdb.domdbapi.web.rest.v2.api;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.annotation.PostConstruct;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se.rivta.domdb.domdbapi.Application;

@ActiveProfiles(profiles = "dev")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
@Rollback(false)
public class ServiceContractAPIControllerTest extends ServiceContractAPIControllerTestBase{

	@PostConstruct
	public void setup() {
		super.setup(controller);
	}

	@Before
	public void prepareDatabase() {
		super.prepareDatabase();
	}

	@After
	public void clearDatabase(){
		super.clearDatabase();
	}
	
	@Test
	public void filterOnMajorAndMinor() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?minor=1&major=2");

		containsNot(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		contains(content, serviceContract3.getName());
	}
	
	@Test
	public void filterOnName() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?name=" + serviceContract.getName());

		contains(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		containsNot(content, serviceContract3.getName());
	}
	
	@Test
	public void filterOnMajor() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?major=1");

		contains(content, serviceContract.getName());
		contains(content, serviceContract2.getName());
		containsNot(content, serviceContract3.getName());
	}
	
	@Test
	public void filterOnMinor() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?minor=1");

		contains(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		contains(content, serviceContract3.getName());
	}
	
	@Test
	public void filterOnNamespace() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?namespace=" + serviceContract3.getNamespace());

		containsNot(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		contains(content, serviceContract3.getName());
	}
		
	@Test
	public void getAllAsJson() throws Exception {
		String content = doRequestForJSON("/v2/servicecontracts.json");

		contains(content, serviceContract.getName());
		contains(content, serviceContract2.getName());
		contains(content, serviceContract3.getName());
	}

	@Test
	public void getAllAsXml() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts");

		contains(content, serviceContract.getName());
		contains(content, serviceContract2.getName());
		contains(content, serviceContract3.getName());
	}

	@Test
	public void getOne() throws Exception{
		String content = doRequestForXML("/v2/servicecontracts/" + serviceContract.getId());
		contains(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		containsNot(content, serviceContract3.getName());
	}

	@Test
	public void getOneAsJsone() throws Exception{
		String content = doRequestForXML("/v2/servicecontracts.json/" + + serviceContract2.getId());
		containsNot(content, serviceContract.getName());
		contains(content, serviceContract2.getName());
		containsNot(content, serviceContract3.getName());
	}

	@Test
	public void getAllPagingSortByName() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?size=1&page=0&sort=name");

		contains(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		containsNot(content, serviceContract3.getName());

	}

	@Test
	public void getAllPagingSortByNameDESC() throws Exception {
		String content = doRequestForXML("/v2/servicecontracts?size=2&page=1&sort=namespace,DESC");

		contains(content, serviceContract.getName());
		containsNot(content, serviceContract2.getName());
		containsNot(content, serviceContract3.getName());
	}

}
