/**
 * 
 */
package se.rivta.domdb.domdbapi.service;

import se.rivta.domdb.domain.core.Domain;

/**
 * @author muqkha
 *
 */
public interface DomainTestService extends DomainService {
	
	Domain save(Domain domain);
	void delete(Domain domain);
}
