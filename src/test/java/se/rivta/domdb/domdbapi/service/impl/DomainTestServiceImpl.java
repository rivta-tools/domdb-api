package se.rivta.domdb.domdbapi.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import se.rivta.domdb.domain.core.Domain;
import se.rivta.domdb.domdbapi.service.DomainTestService;

/**
 * @author muqkha
 *
 */
@Service("domainTestService")
@Transactional(readOnly = true)
public class DomainTestServiceImpl extends DomainServiceImpl implements DomainTestService {

	@Override
	public Domain save(Domain domain) {
		return domainRepository.save(domain);
	}

	@Override
	public void delete(Domain domain){
		domainRepository.delete(domain);
	}

}
