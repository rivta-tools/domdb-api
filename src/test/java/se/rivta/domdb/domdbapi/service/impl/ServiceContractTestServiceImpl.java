package se.rivta.domdb.domdbapi.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import se.rivta.domdb.domain.core.ServiceContract;
import se.rivta.domdb.domdbapi.service.ServiceContractTestService;

/**
 * @author muqkha
 *
 */
@Service("serviceContractTestService")
@Transactional(readOnly = true)
public class ServiceContractTestServiceImpl extends ServiceContractServiceImpl implements ServiceContractTestService {
	
	@Override
	public ServiceContract save(ServiceContract serviceContract) {
		return serviceContractRepository.save(serviceContract);
	}

	@Override
	public void delete(ServiceContract serviceContract) {
		serviceContractRepository.delete(serviceContract);
	}
}
