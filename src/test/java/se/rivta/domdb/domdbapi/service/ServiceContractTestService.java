/**
 * 
 */
package se.rivta.domdb.domdbapi.service;

import se.rivta.domdb.domain.core.ServiceContract;

/**
 * @author muqkha
 *
 */
public interface ServiceContractTestService {

	ServiceContract save(ServiceContract serviceContract);
	void delete(ServiceContract serviceContract);
}
