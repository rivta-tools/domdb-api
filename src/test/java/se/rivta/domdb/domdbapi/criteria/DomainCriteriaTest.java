package se.rivta.domdb.domdbapi.criteria;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DomainCriteriaTest {

	@Before
	public void setUp() throws Exception {

	}
	
	@Test
	public void isEmpty_shouldBeEmpty() throws Exception {
		DomainCriteria dc = new DomainCriteria();
		assertTrue(dc.isEmpty());
	}

	@Test
	public void isEmpty_shouldNotBeEmpty() throws Exception {
		DomainCriteria dc = new DomainCriteria();
		dc.setName("Name");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setDescription("Description");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setSwedishLong("SwedishLong");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setSwedishShort("SwedishShort");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setOwner("Owner");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setHidden(true);
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setIssueTrackerUrl("IssueTrackerUrl");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setSourceCodeUrl("SourceCodeUrl");
		assertFalse(dc.isEmpty());
		
		dc = new DomainCriteria();
		dc.setInfoPageUrl("InfoPageUrl");
		assertFalse(dc.isEmpty());

		dc = new DomainCriteria();
		dc.setDomainType(1);
		assertFalse(dc.isEmpty());
	}

}
