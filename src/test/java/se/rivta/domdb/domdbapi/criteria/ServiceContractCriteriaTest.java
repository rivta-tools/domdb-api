package se.rivta.domdb.domdbapi.criteria;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ServiceContractCriteriaTest {

	@Before
	public void setUp() throws Exception {

	}
	
	@Test
	public void isEmpty_shouldBeEmpty() throws Exception {
		ServiceContractCriteria dc = new ServiceContractCriteria();
		assertTrue(dc.isEmpty());
	}

	@Test
	public void isEmpty_shouldNotBeEmpty() throws Exception {
		ServiceContractCriteria dc = new ServiceContractCriteria();
		dc.setName("Name");
		assertFalse(dc.isEmpty());
		
		dc = new ServiceContractCriteria();
		dc.setNamespace("Namespace");
		assertFalse(dc.isEmpty());
		
		dc = new ServiceContractCriteria();
		dc.setMajor((short)1);
		assertFalse(dc.isEmpty());
		
		dc = new ServiceContractCriteria();
		dc.setMinor((short)2);
		assertFalse(dc.isEmpty());
	}

}
