## Systemkrav
Domdb-api-komponenterna har f�ljande systemkrav:

Programvara       | Driftsmilj� | Utvecklingsmilj�
------ | ----------- | -----------------
Java   | JRE 7 eller 8 | JDK 7 eller 8
MySQL  | 5.5+        | 5.5+
Maven  | Beh�vs ej   | 3+
Tomcat | 7           | Beh�vs ej

## Beroenden
  * Domdb-api �r beroende av domdb-domain och d� fr�mst dess databas.

## Testad konfiguration
Verktygen har testats med f�ljande versioner:

  * Ubuntu 14.04/Windows 8/Linux 
  * Java OpenJDK 7
  * MySQL 5.5.29 och 5.5.41
  * Maven 3.0.5

## Installation
Installera f�ljande paket:

  * mysql-server-5.5 (kan f�rst�s �ven ligga p� en separat server - i s� fall installera ist�llet mysql-client-5.5 eller -5.6)
  * maven (installerar �ven java openjdk-7-jdk)

S�tt Groovy environment-variablerna i filen ~/.profile

## Forts�ttning
Du kan nu forts�tta till [DeveloperInstructions](DeveloperInstructions.md).