
Utvecklarinstruktioner

Dessa instrunktioner f�ruts�tter att dombd-domain projektet �r uppsatt d� dess databas beh�vs f�r detta projekt.

Programvaror

Du beh�ver f�ljande programvaror installerade f�r att kunna k�ra koden i "utvecklarl�ge", samt f�r att paketera releaser:

    Java Development Kit (version 1.7+) - beh�ver vara installerad och milj�variabeln JAVA_HOME satt till JDK-installationens rotkatalog (ett steg ovanf�r javas bin-katalog)
    Maven (version 3+) - beh�ver vara uppackas och dess bin-katalog finnas med i PATH
    MySQL (version 5.5+) - beh�ver vara installerad och du ha ett konto med h�g beh�righet

Konfigurera utvecklingsmilj�n
Klona Git-repository

Klona detta git-repository via en Git-klient, alternativt ladda ned k�llkoden som en zip.
Bygg med Maven

mvn clean install

Om man har problem att bygga p� grunda av PermGen s�tta MAVEN_OPTS set MAVEN_OPTS=-Xmx512m -XX:MaxPermSize=128m

Permanentl�sning: skapa fil mavenrc_pre.bat under %HOME% l�gga MAVEN_OPTS

Skapa en katalog f�r domdb konfigurationsfiler n�gonstans, och peka ut s�kv�gen via milj�variabeln domdb_config_dir. Detta b�r vara gjort n�r dombd-domain sattes upp.

Skapa inst�llningsfil:
domdbapi-config-override.properties
filen skall inneh�lla f�ljande information:
spring.datasource.url=jdbc:mysql://<host-url>/<database-name>?autoReconnect=true
spring.datasource.username=<user>
spring.datasource.password=<password>
spring.datasource.testOnBorrow=true
spring.datasource.validationQuery=SELECT 1

Nu kan du bygga en war-fil med f�ljande kommando:

mvn package

war-filen skapas i katalogen domdb-api/target/

Checka in �ndringar

Om allt ser bra ut:

    committa dina �ndringar till Git,
    skapa en tag med samma namn som versionsnumret
    pusha alltsammans till Bitbucket (dubbelkolla att taggen syns p� Bitbucket).
